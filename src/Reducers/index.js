import { combineReducers } from "redux";
import NavigationReducer from "./navigationReducer";
import CounterReducer from "./counterReducer";

const AppReducer = combineReducers({
  NavigationReducer,
  CounterReducer
});

export default AppReducer;
