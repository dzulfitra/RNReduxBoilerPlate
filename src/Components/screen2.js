import React, { Component } from "react";
import { Text, TouchableOpacity, View } from "react-native";

export default class Screen2 extends Component {
  static navigationOptions = {
    title: "Screen 2"
  };

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "powderblue" }}>
        <Text>{this.props.navigation.state.params.name}</Text>
      </View>
    );
  }
}
